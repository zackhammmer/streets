<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <form action="address" method="get">
            <label for="address">Введите адрес: </label>
            <input type="text" id="address" name="address" accept-charset="UTF-8">
            <button type="submit">Submit</button>
    </form>
    <p>Количество найденых улиц по запросу `${address}`: ${numberOfStreets}</p>

    <p>Список найденых улиц:</p>
    <%-- Не получилось вывести в другом виде --%>
    <p>${list}</p>
</body>
</html>