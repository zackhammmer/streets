package com.kkb.streets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

@SpringBootApplication
public class StreetsApplication {

	public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
		SpringApplication.run(StreetsApplication.class, args);

		System.setProperty("java.awt.headless", "false");

		URI uri= new URI("http://localhost:8080/address");
		java.awt.Desktop.getDesktop().browse(uri);
	}

}
