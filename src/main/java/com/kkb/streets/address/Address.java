package com.kkb.streets.address;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private List<Locations> locations = new ArrayList<>();
    private FromBound from_bound;
    private ToBound to_bound;
    private boolean restrict_value;
    private String query;
}

