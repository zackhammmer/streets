package com.kkb.streets.service;

import com.google.gson.*;
import com.kkb.streets.address.Address;
import com.kkb.streets.address.FromBound;
import com.kkb.streets.address.Locations;
import com.kkb.streets.address.ToBound;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class AddressService {

    @Value("${dadata.apiToken}")
    private String apiToken;

    @Value("${dadata.url}")
    private String url;

    @Value("${dadata.krdFiasId}")
    private String krdFiasId;

    public HttpResponse<String> sendRequest(String addr) throws IOException, InterruptedException, URISyntaxException {

        // Объект для тела запроса
        Address address = new Address();
        address.setLocations(List.of(new Locations(krdFiasId)));
        address.setFrom_bound(new FromBound("street"));
        address.setTo_bound(new ToBound("street"));
        address.setRestrict_value(true);
        address.setQuery(addr);

        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        String jsonRequestBody = gson.toJson(address);

        // Запрос к dadata
        HttpRequest postRequest = HttpRequest.newBuilder()
                .uri(new URI(url))
                .header("Authorization", "Token " + apiToken)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(jsonRequestBody))
                .build();

        HttpClient httpClient = HttpClient.newHttpClient();

        HttpResponse<String> response =  httpClient.send(postRequest, HttpResponse.BodyHandlers.ofString());
        return response;
    }

    public int getNumberOfStreets(HttpResponse<String> resp) throws URISyntaxException, IOException, InterruptedException {

        String[] strList = resp.body().split("\"value\"");

        // Минус один, потому что при делении по value самый первый элемент есть всегда и значения он не имеет
        return strList.length - 1;
    }

    public List<String> getListOfStreets(HttpResponse<String> resp) {

        List<String> listOfStreets = new ArrayList<>();

        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        JsonElement jsonElementBody = gson.fromJson(resp.body(), JsonElement.class);
        JsonObject jsonObjectBody = jsonElementBody.getAsJsonObject();
        JsonArray jsonArrayOfBody = jsonObjectBody.getAsJsonArray("suggestions");
        JsonElement data = null;

        // Проверяем, пустой ли ответ и выводим названия найденных улиц
        if (!jsonArrayOfBody.isEmpty()) {
            for (int i = 0; i < jsonArrayOfBody.size(); i ++) {
                data = jsonArrayOfBody.get(i);
                listOfStreets.add(String.valueOf(data.getAsJsonObject().get("value")));
            }
        }

        return listOfStreets;

    }
}
