package com.kkb.streets.controller;

import com.kkb.streets.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;

@Controller
public class AddressController {

    @Autowired
    AddressService addressService;
    @GetMapping("/address")
    public String address(@ModelAttribute("address") String address, Model model) throws URISyntaxException, IOException, InterruptedException {
        model.addAttribute("address", address);
        HttpResponse<String>  response = addressService.sendRequest(address);
        model.addAttribute("numberOfStreets", addressService.getNumberOfStreets(response));
        model.addAttribute("list", addressService.getListOfStreets(response));
        return "index";
    }
}
